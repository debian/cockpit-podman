cockpit-podman (102-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Thu, 27 Feb 2025 09:34:48 +0000

cockpit-podman (101-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Thu, 13 Feb 2025 10:06:12 +0000

cockpit-podman (100-1) unstable; urgency=medium

  * New upstream release:
   - Automatically start podman.socket
   - Identify quadlets by a 'service' label

 -- Martin Pitt <mpitt@debian.org>  Wed, 29 Jan 2025 10:25:36 +0000

cockpit-podman (99-2) unstable; urgency=medium

  * autopkgtest: Drop unit start test with LXC on Debian 12.
    The attempted workaround does not suffice on LXC 5 on Debian 12 any
    more (see #1073815). The units fail with "Failed to update dynamic user
    credentials: Permission denied". Also, this was becoming ridiculous, and
    not really testing the package any more.
    Give up and skip that part of the text under these conditions. It will
    still run in QEMU runners (such as in Ubuntu or some Debian
    architectures).

 -- Martin Pitt <mpitt@debian.org>  Wed, 11 Dec 2024 00:04:56 +0000

cockpit-podman (99-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Wed, 20 Nov 2024 16:13:16 +0100

cockpit-podman (98-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Wed, 06 Nov 2024 14:10:57 +0100

cockpit-podman (97-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Wed, 23 Oct 2024 10:54:00 +0200

cockpit-podman (96-1) unstable; urgency=medium

  * New upstream release:
    - Pull images from registries without search API

 -- Martin Pitt <mpitt@debian.org>  Wed, 09 Oct 2024 14:56:12 +0000

cockpit-podman (95-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Wed, 25 Sep 2024 12:55:37 +0200

cockpit-podman (94-1) unstable; urgency=medium

  * New upstream release:
    - Render ports are ranges in container integration tab
  * Add git-buildpackage configuration. In particular, enable pristine-tar.

 -- Martin Pitt <mpitt@debian.org>  Sun, 15 Sep 2024 17:55:33 +0200

cockpit-podman (92-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Thu, 08 Aug 2024 11:14:39 +0200

cockpit-podman (91-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Wed, 10 Jul 2024 11:49:19 +0200

cockpit-podman (90-1) unstable; urgency=medium

  * New upstream release:
    - Implement pull option for existing images

 -- Martin Pitt <mpitt@debian.org>  Wed, 26 Jun 2024 14:21:16 +0200

cockpit-podman (89-4) unstable; urgency=medium

  * autopkgtest: Adjust DynamicUser= workaround.
    cockpit dropped the static adduser from its postinst and moved the hack
    into its autopkgtest. Follow suit. Also restrict the hack to running
    inside of a container, as it works fine on the host. Add bug reference.

 -- Martin Pitt <mpitt@debian.org>  Wed, 19 Jun 2024 08:08:55 +0200

cockpit-podman (89-3) unstable; urgency=medium

  * autopkgtest: Adjust for test running as root
  * autopkgtest: Add logs for debugging failure

 -- Martin Pitt <mpitt@debian.org>  Mon, 10 Jun 2024 06:22:34 +0200

cockpit-podman (89-2) unstable; urgency=medium

  * autopkgtest: Add workaround for stable kernel+LXC breaking DynamicUser=.
    Our autopkgtest fails in ci.debian.net with "Failed to update dynamic user
    credentials: Permission denied".
    This is a bug with the Debian 12 kernel and LXC, and doesn't reproduce
    when running on the host with the old kernel, or running LXC+kernel on
    testing. We don't even *actually* use a DynamicUser= there, it's statically
    allocated.
    Taken from https://salsa.debian.org/utopia-team/cockpit/-/commit/36c1e44

 -- Martin Pitt <mpitt@debian.org>  Sun, 09 Jun 2024 09:23:43 +0200

cockpit-podman (89-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Sat, 08 Jun 2024 10:28:02 +0200

cockpit-podman (88-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Mon, 03 Jun 2024 06:52:22 +0200

cockpit-podman (87-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Fri, 26 Apr 2024 07:39:34 +0200

cockpit-podman (86-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Sat, 30 Mar 2024 12:29:42 +0100

cockpit-podman (85-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Wed, 13 Mar 2024 13:57:11 +0100

cockpit-podman (84-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Wed, 14 Feb 2024 14:04:20 +0000

cockpit-podman (83-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Thu, 18 Jan 2024 15:14:49 +0100

cockpit-podman (82-1) unstable; urgency=medium

  * New upstream release:
    - Delete intermediate images

 -- Martin Pitt <mpitt@debian.org>  Wed, 29 Nov 2023 10:15:03 +0000

cockpit-podman (81-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Thu, 16 Nov 2023 07:42:14 +0100

cockpit-podman (80-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Thu, 02 Nov 2023 09:50:03 +0100

cockpit-podman (79-1) unstable; urgency=medium

  * New upstream release:
    - Validate fields in "Create container" dialog

 -- Martin Pitt <mpitt@debian.org>  Thu, 19 Oct 2023 08:05:28 +0200

cockpit-podman (78-1) unstable; urgency=medium

  * New upstream release:
    - Label Toolbox and Distrobox containers

 -- Martin Pitt <mpitt@debian.org>  Mon, 09 Oct 2023 09:36:37 +0200

cockpit-podman (77-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Thu, 21 Sep 2023 07:15:10 +0200

cockpit-podman (76-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Wed, 06 Sep 2023 16:03:34 +0200

cockpit-podman (75-1) unstable; urgency=medium

  * New upstream bug fix release.
    - Remove po/LINGUAS during package cleaning (Closes: #1043810)

 -- Martin Pitt <mpitt@debian.org>  Wed, 23 Aug 2023 13:00:05 +0200

cockpit-podman (74-1) unstable; urgency=medium

  * New upstream release:
    - Update to PatternFly 5
    - Bug fixes and translation updates

 -- Martin Pitt <mpitt@debian.org>  Wed, 09 Aug 2023 23:25:32 +0200

cockpit-podman (73-1) unstable; urgency=medium

  * New upstream release:
    - Show time of container's latest checkpoint

 -- Martin Pitt <mpitt@debian.org>  Wed, 26 Jul 2023 10:23:36 +0000

cockpit-podman (72-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Wed, 12 Jul 2023 07:56:44 +0000

cockpit-podman (71-1) unstable; urgency=medium

  * New upstream release:
    - Update to PatternFly 5 Alpha
    - Add ability to prune unused containers
    - Add manifest condition for the Python bridge

 -- Martin Pitt <mpitt@debian.org>  Sat, 17 Jun 2023 09:11:59 +0000

cockpit-podman (67-1) unstable; urgency=medium

  * New upstream release:
    - Container list can be sorted
    - Custom healthcheck actions

 -- Martin Pitt <mpitt@debian.org>  Wed, 19 Apr 2023 12:10:55 +0000

cockpit-podman (65-1) unstable; urgency=medium

  * New upstream bug fix release:
    - Show dialog errors at the top of the dialogs
  * Add autopkgtest. This checks that the manifest and data files are
    correctly installed and loaded by Cockpit.

 -- Martin Pitt <mpitt@debian.org>  Thu, 30 Mar 2023 08:10:56 +0200

cockpit-podman (64-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Fri, 10 Mar 2023 11:41:55 +0100

cockpit-podman (63-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Fri, 03 Mar 2023 05:31:10 +0100

cockpit-podman (62-1) unstable; urgency=medium

  * New upstream bug fix releae.

 -- Martin Pitt <mpitt@debian.org>  Sun, 12 Feb 2023 07:53:40 +0100

cockpit-podman (61-1) unstable; urgency=medium

  * New upstream release:
    - Use container image's default command
    - Fix tabular numbers font

 -- Martin Pitt <mpitt@debian.org>  Wed, 25 Jan 2023 16:47:26 +0100

cockpit-podman (60-1) unstable; urgency=medium

  * New upstream release:
    - Patternfly update and other maintenance

 -- Martin Pitt <mpitt@debian.org>  Thu, 12 Jan 2023 10:26:43 +0100

cockpit-podman (59-1) unstable; urgency=medium

  * New upstream release:
    - Start using tabular fonts
    - Other UI fixes and improvements
  * debian/watch: Use GitHub API

 -- Martin Pitt <mpitt@debian.org>  Sun, 25 Dec 2022 08:03:04 +0100

cockpit-podman (57-1) unstable; urgency=medium

  * New upstream bug fix release.

 -- Martin Pitt <mpitt@debian.org>  Mon, 21 Nov 2022 15:26:44 +0100

cockpit-podman (56-1) unstable; urgency=medium

  [ Martin Pitt ]
  * New upstream release:
    - Dark theme support

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Martin Pitt <mpitt@debian.org>  Tue, 08 Nov 2022 10:05:37 +0100

cockpit-podman (55-1) unstable; urgency=medium

  * New upstream release:
    - Pod CPU, memory, port and volume details
    - Create new pod group functionality

 -- Martin Pitt <mpitt@debian.org>  Sat, 29 Oct 2022 07:24:06 +0200

cockpit-podman (54-1) unstable; urgency=medium

  * New upstream release:
    - Show all containers by default

 -- Martin Pitt <mpitt@debian.org>  Fri, 23 Sep 2022 06:43:36 +0200

cockpit-podman (53-1) unstable; urgency=medium

  * New upstream release:
    - Add Volumes and Env Variables to container details
    - Show volume permission in container integration tab
    - Allow no system users to set restart policy
    - Show image history

 -- Martin Pitt <mpitt@debian.org>  Wed, 07 Sep 2022 10:22:48 +0200

cockpit-podman (51-1) unstable; urgency=medium

  * New upstream release:
    - Fix events with large number of containers
    - Fix tests for Python 3.11
    - Translation updates

 -- Martin Pitt <mpitt@debian.org>  Wed, 20 Jul 2022 13:42:42 +0200

cockpit-podman (50-1) unstable; urgency=medium

  * New upstream release:
    - Use NumberInput for Image Run Dialog

 -- Martin Pitt <mpitt@debian.org>  Thu, 23 Jun 2022 13:33:36 +0200

cockpit-podman (49.1-1) unstable; urgency=medium

  * New upstream release:
    - Container renaming
    - Health check support

 -- Martin Pitt <mpitt@debian.org>  Thu, 09 Jun 2022 11:07:15 +0200

cockpit-podman (46-1) unstable; urgency=medium

  * New upstream release:
    - Translation updates

 -- Martin Pitt <mpitt@debian.org>  Wed, 13 Apr 2022 09:44:11 +0200

cockpit-podman (45-1) unstable; urgency=medium

  * New upstream release:
    - Translation updates

 -- Martin Pitt <mpitt@debian.org>  Wed, 30 Mar 2022 16:13:55 +0200

cockpit-podman (44-1) unstable; urgency=medium

  * New upstream release:
    - Always use base 10 size units
    - Move owner option to details tab

 -- Martin Pitt <mpitt@debian.org>  Tue, 22 Mar 2022 06:15:17 +0100

cockpit-podman (42-1) unstable; urgency=medium

  * New upstream release:
    - Tests improvements and stabilization

 -- Martin Pitt <mpitt@debian.org>  Wed, 16 Feb 2022 16:46:50 +0100

cockpit-podman (41-1) unstable; urgency=medium

  * New upstream release:
    - Adjust for podman 4.0 API break
    - Improve page layout on mobile devices

 -- Martin Pitt <mpitt@debian.org>  Wed, 02 Feb 2022 14:32:38 +0100

cockpit-podman (40-1) unstable; urgency=medium

  * New upstream release:
    - Add pause/resume to containers
    - Always pull the latest image when creating a new container

 -- Martin Pitt <mpitt@debian.org>  Tue, 25 Jan 2022 08:38:53 +0100

cockpit-podman (39-1) unstable; urgency=medium

  * New upstream release:
    - Create container in pod
    - Podman restart policy
    - Allow inserting multiple environment variables

 -- Martin Pitt <mpitt@debian.org>  Wed, 05 Jan 2022 16:16:19 +0100

cockpit-podman (38-1) unstable; urgency=medium

  * New upstream release:
    - Updated translations
    - Consistent colors for pod and container running status

 -- Martin Pitt <mpitt@debian.org>  Thu, 09 Dec 2021 17:14:48 +0100

cockpit-podman (37-1) unstable; urgency=medium

  * New upstream release:
    - Improved image commit UI
    - PatternFly updates and fixes

 -- Martin Pitt <mpitt@debian.org>  Wed, 24 Nov 2021 16:09:55 +0100

cockpit-podman (36-1) unstable; urgency=medium

  * New upstream release:
    - New "Create container" workflow
    - Prune unused images

 -- Martin Pitt <mpitt@debian.org>  Wed, 10 Nov 2021 11:54:30 +0100

cockpit-podman (34-1) unstable; urgency=medium

  * New upstream release:
    - First iteration of page redesign

 -- Martin Pitt <mpitt@debian.org>  Wed, 01 Sep 2021 18:34:55 +0200

cockpit-podman (33-1) unstable; urgency=medium

  * New upstream release:
    - Fix crash with "Used Images" links
    - Translation updates

 -- Martin Pitt <mpitt@debian.org>  Sun, 22 Aug 2021 07:58:00 +0200

cockpit-podman (29-1) unstable; urgency=medium

  * New upstream bug fix release:
    - PatternFly 4 updates for a more consistent UI
    - Accessibility fixes
  * Bump Standards-Version to 4.5.1.  No changes necessary.

 -- Martin Pitt <mpitt@debian.org>  Wed, 17 Mar 2021 08:46:26 +0100

cockpit-podman (28-1) unstable; urgency=medium

  * New upstream version:
    - Drop cockpit-system dependency
    - Correctly show selected option for SELinux labels

 -- Martin Pitt <mpitt@debian.org>  Thu, 04 Feb 2021 13:05:34 +0100

cockpit-podman (27.1-1) unstable; urgency=medium

  * New upstream version.

 -- Martin Pitt <mpitt@debian.org>  Fri, 15 Jan 2021 06:00:51 +0100

cockpit-podman (25-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No change source-only upload.

 -- Boyuan Yang <byang@debian.org>  Thu, 14 Jan 2021 11:43:51 -0500

cockpit-podman (25-1) unstable; urgency=medium

  * Initial release (Closes: #967030)

 -- Martin Pitt <mpitt@debian.org>  Fri, 16 Oct 2020 16:28:02 +0200
